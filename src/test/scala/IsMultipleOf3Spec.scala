import org.scalatest._
import Quizzes._

/**
  * Created by mark on 05/03/2017.
  */
class   IsMultipleOf3Spec extends FunSuite{


  test("111 is multiple of 3"){
    assert(isMultipleOf3(111  ))
  }

  test("0 is multiple of 3"){
    assert(isMultipleOf3(0))
  }
  test("4 is not multiple of 3"){
    assert(!isMultipleOf3(4))
  }
}
